package app.shop.coffee.com.coffeeshop;

import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class FoodCategoryActivity extends AppCompatActivity {

    private SQLiteDatabase db;
    private Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_food_category);

        //Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Up navigation
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        ListView listFoods = (ListView) findViewById(R.id.list_foods);

        CoffeeShopDatabaseHelper mDbHelper  = new CoffeeShopDatabaseHelper(this);

        try{

            db = mDbHelper.getReadableDatabase();
            cursor = db.query("FOOD", new String[]{"_id", "NAME"},null,null,null,null, null);

            SimpleCursorAdapter listAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1,cursor,new String[]{"NAME"},new int[]{android.R.id.text1},0);

            listFoods.setAdapter(listAdapter);
        }
        catch (SQLException e)
        {
            Toast toast = Toast.makeText(this, "Database Unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        //Create the listener

        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listFoods, View itemView, int position, long id) {

                //Pass the food user clicks onto FoodActivtiy
                Intent intent = new Intent(FoodCategoryActivity.this, FoodActivity.class);

                intent.putExtra(FoodActivity.EXTRA_FOODID, (int) id);
                startActivity(intent);

            }
        };

        //Assign the listener to the list view
        listFoods.setOnItemClickListener(itemClickListener);

    }

    public void onDestroy()
    {
        super.onDestroy();
        cursor.close();
        db.close();
    }
}
